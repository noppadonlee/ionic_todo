const addBtn = document.getElementById('add-btn');
const resetBtn = document.getElementById('reset-btn');
const todoInput = document.getElementById('todo-input');
const resultArea = document.getElementById('result');

const resetInputs = () => {
  todoInput.value = '';
};

const addTodo = () => {
  const enteredTodo = todoInput.value;

  if (enteredTodo == '') {
    alert('กรุณาใส่สิ่งที่ต้องการบันทึก');
    return;
  }

  const resultElement = document.createElement('ion-item');
  resultElement.innerHTML = `<p>${enteredTodo}</p>`;
  // resultArea.innerHTML = '';
  resultArea.appendChild(resultElement);
  todoInput.value = '';
};

addBtn.addEventListener('click', addTodo);
resetBtn.addEventListener('click', resetInputs);